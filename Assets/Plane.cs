using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    public TMPro.TMP_Text thrustText;
    public Rigidbody physicsBody;
    public Controller controller;

    public LayerMask goundLayer;
    public Transform groundCheck;

    [Header("Controls")]
    public bool Engine1 = true;
    public bool Engine2 = true;
    public bool CRASH = false;
    public bool IsBreaksOn;
    public bool AutoPilot = false;

    [Header("Info")]
    public int Altitude = 0;
    public decimal Thrust = 0.0m;
    public float Yaw = 0.0f;
    public float Pitch = 0.0f;
    public float Roll = 0.0f;
    public bool AutoPilotError = false;
    public bool Engine1Fire = false;
    public bool Engine2Fire = false;


    // Control Settings
    private decimal thrustStep = 0.2m;
    public decimal maxSpeed = 20;
    private float yawAmount = 120f; // Speed of turning
    private float pitchAmount = 20f; // Maximum and minimum pitch allowed
    private float rollAmount = 30f; // Maximum and minimum roll allowed

    private decimal _maxSpeed;


    // Player Input
    public float horizontalInput = 0.0f;
    public float verticalInput = 0.0f;
    public float rudderInput = 0.0f;

    public bool isGrounded = false;

    private void FixedUpdate()
    {
        if (isGrounded)
        {
            physicsBody.isKinematic = true;
            return;
        }

        if (Thrust <= 0.0m || (!Engine1 && !Engine2) || IsBreaksOn)
        {
            physicsBody.isKinematic = false;
        }
        else
        {
            physicsBody.isKinematic = true;
        }
    }

    private void Update()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"));

        if (hit.distance <= 0 && !isGrounded)
            Altitude = System.Convert.ToInt32(transform.position.y);
        else
            Altitude = System.Convert.ToInt32(hit.distance);

        if (physicsBody.velocity.y < -10f && isGrounded)
        {
            CRASH = true;
        }

        if (CRASH)
            return;

        isGrounded = Physics.CheckSphere(groundCheck.position, 0.2f, goundLayer);

        if (isGrounded)
        {
            _maxSpeed = 4;
            if (Thrust > _maxSpeed)
                Thrust = _maxSpeed;
        }
        else
            _maxSpeed = maxSpeed;

        UpdateUI();

        HandleInput();

        if (AutoPilotError)
            AutoPilot = false;

        if (!Engine1 && !Engine2)
        {
            if (AutoPilot) AutoPilotError = true;
            return;
        }

        if ((!Engine1 || !Engine2) && AutoPilot)
            AutoPilotError = true;

        if ((Engine1Fire || Engine2Fire) && AutoPilot)
            AutoPilotError = true;

        PlaneMovement();
    }

    private void PlaneMovement()
    {
        if (IsBreaksOn)
            return;

        // Move in forward direction
        transform.position += transform.forward * (float)Thrust * Time.deltaTime;

        if (isGrounded)
            GroundMovement();
        else
            InAirMovement();
    }

    private void GroundMovement()
    {
        if (Thrust <= 0)
            return;

        // YAW (Left and Right) - Y Axis
        Yaw += rudderInput * (yawAmount / 4) * Time.deltaTime;

        // Pitch (Up and Down) - X Axis
        if (Thrust >= 2)
        {
            Pitch -= verticalInput;
            Pitch = Mathf.Clamp(Pitch, -20, 0);
        }
        else
        {
            Pitch = 0;
        }

        transform.localRotation = Quaternion.Euler(Vector3.up * Yaw + Vector3.right * Pitch);
    }

    private void InAirMovement()
    {
        if (AutoPilot)
        {
            Pitch = 0.0f;
            Roll = 0.0f;
            transform.localRotation = Quaternion.Euler(Vector3.up * Yaw + Vector3.zero + Vector3.zero);
            return;
        }

        // YAW (Left and Right) - Y Axis
        Yaw += horizontalInput * yawAmount * Time.deltaTime;
        Yaw += rudderInput * (yawAmount / 4) * Time.deltaTime;

        // Pitch (Up and Down) - X Axis
        //Pitch = Mathf.Lerp(0, 20, Mathf.Abs(verticalInput)) * Mathf.Sign(verticalInput); This is for button input
        Pitch -= verticalInput;
        Pitch = Mathf.Clamp(Pitch, -pitchAmount, pitchAmount);

        // Roll - Z Axis
        //Roll = Mathf.Lerp(0, 30, Mathf.Abs(horizontalInput)) * -Mathf.Sign(horizontalInput); This is for button input
        Roll -= horizontalInput;
        Roll = Mathf.Clamp(Roll, -rollAmount, rollAmount);

        transform.localRotation = Quaternion.Euler(Vector3.up * Yaw + Vector3.right * Pitch + Vector3.forward * Roll);
    }

    private void UpdateUI()
    {
        thrustText?.SetText($"THRUST: {System.Convert.ToInt32((Thrust / _maxSpeed) * 100)}%");
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Engine1 = !Engine1;
            Engine2 = !Engine2;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AutoPilotError = !AutoPilotError;
        }

        if (AutoPilot)
            return;

        if (Input.GetKey(KeyCode.W) && Thrust < _maxSpeed && (Engine1 || Engine2))
            Thrust += thrustStep;
        else if (Input.GetKey(KeyCode.S) && (Engine1 || Engine2))
        {
            if (Thrust - thrustStep < 0)
                Thrust = 0;
            else
                Thrust -= thrustStep;
        }

        if (controller.mouseX <= 2 && controller.mouseX >= -2)
            horizontalInput = Input.GetAxis("Mouse X");
        //  horizontalInput = 0;
        else
            horizontalInput = controller.mouseX / 1000;
        if (controller.mouseY <= 2 && controller.mouseY >= -2)
            verticalInput = Input.GetAxis("Mouse Y");
        //  verticalInput = 0;
        else
            verticalInput = controller.mouseY / 1000;
        rudderInput = Input.GetAxis("Horizontal");
    }
}
