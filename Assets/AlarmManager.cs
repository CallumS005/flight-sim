using UnityEngine;

public class AlarmManager : MonoBehaviour
{

    public Plane planeSource;

    [Header("Sources")]
    public AudioSource AbortLanding; // Implemented
    public AudioSource AbortTakeoff;
    public AudioSource AutoPilotFail; // Implemented
    public AudioSource Engine1Failure; // Implemented
    public AudioSource Engine2Failure; // Implemented
    public AudioSource FireEngine1; // Implemented
    public AudioSource FireEngine2; // Implemented
    public AudioSource MultiEngineFire; // Implemented
    public AudioSource Stall; // Implemented
    public AudioSource Terrain; // Implemented

    private void Update()
    {
        // Abort Landing
        if (planeSource.Altitude < 40 && planeSource.Thrust > planeSource.maxSpeed / 2 && planeSource.Pitch > 0 && !planeSource.CRASH)
        {
            if (!AbortLanding.isPlaying)
                AbortLanding.Play();
        }
        else
        {
            AbortLanding.Stop();
        }

        // Auto Pilot Fail
        if (!planeSource.isGrounded && planeSource.AutoPilotError)
        {
            if (!AutoPilotFail.isPlaying)
                AutoPilotFail.Play();
        }
        else
        {
            AutoPilotFail.Stop();
        }

        // Engine1 Failure
        if (!planeSource.isGrounded && !planeSource.Engine1 && planeSource.Engine2 || planeSource.CRASH && !planeSource.Engine1 && planeSource.Engine2)
        {
            if (!Engine1Failure.isPlaying)
                Engine1Failure.Play();
        }
        else
        {
            Engine1Failure.Stop();
        }

        // Engine2 Failure
        if (!planeSource.isGrounded && !planeSource.Engine2 && planeSource.Engine1 || planeSource.CRASH && !planeSource.Engine2 && planeSource.Engine1)
        {
            if (!Engine2Failure.isPlaying)
                Engine2Failure.Play();
        }
        else
        {
            Engine2Failure.Stop();
        }

        // Engine1 Fire
        if (planeSource.Engine1Fire && !planeSource.Engine2Fire && planeSource.Engine1)
        {
            if (!FireEngine1.isPlaying)
                FireEngine1.Play();
        }
        else
        {
            FireEngine1.Stop();
        }

        // Engine2 Fire
        if (planeSource.Engine2Fire && !planeSource.Engine1Fire && planeSource.Engine2)
        {
            if (!FireEngine2.isPlaying)
                FireEngine2.Play();
        }
        else
        {
            FireEngine2.Stop();
        }

        // Multi Engine Fire
        if (planeSource.Engine1Fire && planeSource.Engine2Fire && (planeSource.Engine1 || planeSource.Engine2))
        {
            if (!MultiEngineFire.isPlaying)
                MultiEngineFire.Play();
        }
        else
        {
            MultiEngineFire.Stop();
        }

        // Stalling
        if (!planeSource.isGrounded && (!planeSource.Engine1 && !planeSource.Engine2) || planeSource.CRASH && (!planeSource.Engine1 && !planeSource.Engine2))
        {
            if (!Stall.isPlaying)
                Stall.Play();
        }
        else
        {
            Stall.Stop();
        }

        // Terrain 
        if (!planeSource.isGrounded && planeSource.Altitude < 20)
        {
            if (!Terrain.isPlaying)
                Terrain.Play();
        }
        else
        {
            Terrain.Stop();
        }
    }
}
