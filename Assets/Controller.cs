using UnityEngine;
using System.IO.Ports;
using System.Threading;

public class Controller : MonoBehaviour
{
    Thread IOThread = new Thread(DataThread);
    private static SerialPort sp;
    private static string incomingMessage = "";
    private static string outgoingMessage = "";

    public float mouseX;
    public float mouseY;

    private static void DataThread()
    {
        sp = new SerialPort("COM5", 9600);
        sp.Open();

        while (true)
        {
            if (outgoingMessage != "")
            {
                sp.Write(outgoingMessage);
                outgoingMessage = "";
            }

            incomingMessage = sp.ReadExisting();
            Thread.Sleep(200);
        }
    }

    private void Start()
    {
        IOThread.Start();
    }

    private void OnDestroy()
    {
        IOThread.Abort();
        sp.Close();
    }

    private void Update()
    {
        if (incomingMessage != "")
        {
            string[] dataPacket = incomingMessage.Split(",");

            if (dataPacket.Length < 2)
                return;

            float.TryParse(dataPacket[0], out mouseX);
            float.TryParse(dataPacket[1], out mouseY);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
            outgoingMessage = "0";
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            outgoingMessage = "1";
    }
}
